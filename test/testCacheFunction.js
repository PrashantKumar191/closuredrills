const cacheFunction =require('../cacheFunction.js')
const add = (a,b) => a + b;

const cachedAdd = cacheFunction(add);

console.log(cachedAdd(1,2)); 
console.log(cachedAdd(1,2)); 
console.log(cachedAdd(2,1)); 
console.log(cachedAdd(2,3)); 
console.log(cachedAdd(2,3)); 
 
const doMath = (a,b,c) => a + b - c;

const cachedDoMath = cacheFunction(doMath);

console.log(cachedDoMath(1,2,3)); 
console.log(cachedDoMath(1,2,3)); 
console.log(cachedDoMath(1,3,2)); 
console.log(cachedDoMath(2,3,4)); 
console.log(cachedDoMath(2,3,4)); 
 
const square = (num) => num * num;

const cachedSquare = cacheFunction(square);

console.log(cachedSquare(2)); 
console.log(cachedSquare(2)); 
console.log(cachedSquare(3)); 
console.log(cachedSquare(3)); 