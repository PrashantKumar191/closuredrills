const counterFactoryFunction = require('../counterFactory.js')

let result = counterFactoryFunction();
console.log(result.increment());
console.log(result.increment());
console.log(result.increment());
console.log(result.increment());
console.log(result.increment());
console.log(result.decrement());
