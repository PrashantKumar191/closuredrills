function cacheFunction(cb) {
    if(typeof cb !== 'function'){
        return null;
    }
    let cache = [];
    return function(...arg){
        if(arg in cache){
            console.log('From cache');

            return cache[arg];
        } 

        cache[arg] = cb(...arg);
        console.log('first call');

        return cache[arg];
        
    };
}

module.exports = cacheFunction;