function limitFunctionCallCount(cb,n){

    if(typeof cb !== 'function' || typeof n !=='number'){
        return null;
    }
    
    let count = 0;
    
    return function(...args){
        if(count < n){
            count += 1;
            return cb(...args);
        }
        else{
            return null;
        }    
    };
}


module.exports = limitFunctionCallCount;